workspace "Helloworld"
configuration {"Debug", "Release"}


project "Helloworld"
location "Helloworld"
kind "ConsoleApp"
language "C++"
target dir "bin/%{cfg.buildcfg}"

files {"Helloworld/src/**.h", "Helloworld/scr/**.cpp"}

filter "configurations:Debug"
defines {"DEBUG"}
symbols "On"


filter "configurations:Release"
defines {"NDEBUG"}
Optimize "On"

